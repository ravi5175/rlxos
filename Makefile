ARCH ?= x86_64

ARCHDIR := arch/$(ARCH)
KERNELDIR := kernel
LIBCDIR := libc

CFLAGS := -fpermissive

include $(ARCHDIR)/make.config
include $(KERNELDIR)/make.config
include $(LIBCDIR)/make.config

.SUFFIXES: .s .o .ko .lo

.s.o:
	nasm $(ARCH_ASM_FLAGS) $< -o $@

.cc.ko:
	$(CXX) $(CFLAGS) $(KERNEL_CXXFLAGS) -c $< -o $@
	
.c.ko:
	$(CC) $(CFLAGS) $(KERNEL_CFLAGS) -c $< -o $@

.c.lo:
	$(CC) $(LIBC_CFLAGS) -c $< -o $@ -fPIC

rlxnix: $(ARCH_OBJS) $(ARCHDIR)/linker.ld $(KERNEL_OBJS) libk.a
	ld -n -o $@ -T $(ARCHDIR)/linker.ld $(ARCH_OBJS) $(KERNEL_OBJS) libk.a

iso: rlxos.iso

rlxos.iso: rlxnix grub.cfg
	mkdir -p build/iso/boot/grub
	cp grub.cfg build/iso/boot/grub/
	cp rlxnix build/iso/boot/

	grub-mkrescue -o $@ build/iso/

libk.a: $(LIBC_OBJS)
	ar -rc -o $@ $(LIBC_OBJS)

run: rlxos.iso
	qemu-system-x86_64 -cdrom $< -m 512

clean:
	rm -f $(ARCH_OBJS) $(KERNEL_OBJS) $(LIBC_OBJS)
	rm -rf build
	rm -f rlxnix rlxnix.iso libk.a rlxos.iso

