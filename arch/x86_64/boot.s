global X86_ENTRY
extern LONG_MODE_START

section .text
bits 32
X86_ENTRY:
    mov esp, SYS_STACK_TOP
    mov edi, ebx
    mov esi, eax

    ;call CHECK_MULTIBOOT
    call CHECK_CPUID
    call CHECK_LONG_MODE

    call SETUP_PAGE_TABLES
    call ENABLE_PAGING

    cli
    lgdt [GDT64.POINTER]

    jmp GDT64.CODE:LONG_MODE_START
    mov dword[0xB8000], 0x2F4B2F4F
    hlt

ERR:
    mov dword [0xB8000], 0x4F524F45
    mov dword [0xB8004], 0x4F3A4F52
    mov dword [0xB8008], 0x4F204F20
    mov byte  [0xB800A], al
    hlt


CHECK_MULTIBOOT:
    cmp eax, 0x36D76289
    jne .NO_MULTIBOOT
    ret
.NO_MULTIBOOT:
    mov al, "0"
    jmp ERR


CHECK_CPUID:
    pushfd
    pop eax

    mov ecx, eax
    xor eax, 1 << 21

    push eax
    popfd

    pushfd
    pop eax

    push ecx
    popfd

    cmp eax, ecx
    je .NO_CPUID
    ret
.NO_CPUID:
    mov al, "1"
    jmp ERR


CHECK_LONG_MODE:
    mov eax, 0x80000000
    cpuid
    cmp eax, 0x80000001
    jb .NO_LONG_MODE

    mov eax, 0x80000001
    cpuid
    test edx, 1 << 29
    jz .NO_LONG_MODE
    ret
.NO_LONG_MODE:
    mov al, "2"
    jmp ERR

SETUP_PAGE_TABLES:
    mov eax, P3_TABLE
    or eax, 0b11
    mov [P4_TABLE], eax

    mov eax, P2_TABLE
    or eax, 0b11
    mov [P3_TABLE], eax

    mov ecx, 0

.MAP_P2_TABLE:
    ; map ecx-th P2 entry to a huge page that starts at address 2MiB*ecx
    mov eax, 0x200000  ; 2MiB
    mul ecx            ; start address of ecx-th page
    or eax, 0b10000011 ; present + writable + huge
    mov [P2_TABLE + ecx * 8], eax ; map ecx-th entry

    inc ecx            ; increase counter
    cmp ecx, 512       ; if counter == 512, the whole P2 table is mapped
    jne .MAP_P2_TABLE  ; else map the next entry

    ret

ENABLE_PAGING:
    ; load P4 to cr3 register (cpu uses this to access the P4 table)
    mov eax, P4_TABLE
    mov cr3, eax

    ; enable PAE-flag in cr4 (Physical Address Extension)
    mov eax, cr4
    or eax, 1 << 5
    mov cr4, eax

    ; set the long mode bit in the EFER MSR (model specific register)
    mov ecx, 0xC0000080
    rdmsr
    or eax, 1 << 8
    wrmsr

    ; enable paging in the cr0 register
    mov eax, cr0
    or eax, 1 << 31
    mov cr0, eax

    ret


section .rodata
GDT64:
    dq 0
.CODE: equ $ - GDT64
    dq (1<<43) | (1<<44) | (1<<47) | (1<<53)
.POINTER:
    dw $ - GDT64 - 1
    dq GDT64

section .bss
align 4096
P4_TABLE:
    resb 4096

P3_TABLE:
    resb 4096

P2_TABLE:
    resb 4096

SYS_STACK_BOTTOM:
    resb 4096 * 4   ; 16 KiB

SYS_STACK_TOP: