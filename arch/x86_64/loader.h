#pragma once

#include <multiboot.h>
#include <kernel.h>

class MemoryInfo 
{
    public:
        uint32_t lower_addr, upper_addr, size, type;

        MemoryInfo() { }
        MemoryInfo(multiboot_tag* tag)
        {
            lower_addr = ((multiboot_tag_basic_meminfo*)tag)->mem_lower;
            upper_addr = ((multiboot_tag_basic_meminfo*)tag)->mem_upper;
            size       = ((multiboot_tag_basic_meminfo*)tag)->size;
            type       = ((multiboot_tag_basic_meminfo*)tag)->type;
        }

        void print() {
            info("Memory Information");
            print_info("LOWER = %x  UPPER = %x",lower_addr, upper_addr);
            print_info("SIZE = %x TYPE = %d", size, type);
        }
};

struct MemoryMapType
{
    uint64_t address, length;
    uint32_t type, zero;
};

class MemoryMapInfo
{
    public:
        MemoryMapType memoryMap[100];
        size_t size = 0;

        MemoryMapInfo() { }
       MemoryMapInfo(multiboot_tag* tag)
       {
           multiboot_memory_map_t* mmap;
           for(mmap = ((multiboot_tag_mmap*)tag)->entries;
              (multiboot_uint8_t*)mmap < (multiboot_uint8_t*) tag + tag->size;
              mmap = (multiboot_memory_map_t*) (multiboot_uint8_t*)((unsigned long) mmap + ((multiboot_tag_mmap*)tag)->entry_size), size++)
            {
                memoryMap[size].address = mmap->addr;
                memoryMap[size].length  = mmap->len;
                memoryMap[size].type    = mmap->type;
                memoryMap[size].zero    = mmap->zero;
            }
       } 

       void print() {
           info("Memory Map");
           for(size_t i = 0; i < size; i++) {
               print_info("ADDRESS = %x    LENGTH = %x  TYPE = %x   ZERO = %x", memoryMap[i].address, memoryMap[i].length, memoryMap[i].type, memoryMap[i].zero);
           }
       }
};

class FrameBufferInfo {
    public:
        uint64_t address;
        uint32_t height, width;
        uint8_t bits_per_pixel;

    FrameBufferInfo() {}
    FrameBufferInfo(multiboot_tag* tag)
    {
        multiboot_tag_framebuffer* fb = (multiboot_tag_framebuffer*) tag;
        address = fb->common.framebuffer_addr;
        height  = fb->common.framebuffer_height;
        width   = fb->common.framebuffer_width;
        bits_per_pixel = fb->common.framebuffer_bpp;
    }

    void print() {
        info("FrameBuffer");
        print_info("ADDRESS = %x", address);
        print_info("HEIGHT = %d   WIDTH = %d", height, width);
        print_info("BITS PER PIXEL = %d",bits_per_pixel);
    }
};
class BootInfo
{
    private:
        multiboot_tag* __tag__;

    public:
        uint64_t kernel_end_address;
        MemoryInfo memory_info;
        MemoryMapInfo memory_map_info;
        FrameBufferInfo framebuffer_info;

        const char *cmdline, *bootloader_id;

        BootInfo(uint64_t address);
        
};