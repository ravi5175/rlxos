global LONG_MODE_START

section .text
bits 64
LONG_MODE_START:
    
    mov ax, 0
    mov ss, ax
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax

    extern _start

    push rdi
    push rsi
    
    call _start

    mov rax, 0x2f592f412f4b2f4f
    mov qword [0xB8000], rax
    hlt