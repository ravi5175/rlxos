section .X86_MULTIBOOT

X86_START:
    dd 0xE85250D6           ; Magic Number
    dd 0                    ; Protected Mode
    dd X86_END - X86_START  ; Header Length

    ; Checksum
    dd 0x100000000 - (0xe85250d6 + 0 + (X86_END - X86_START))

    dw 0
    dw 0
    dd 8
    
X86_END:
