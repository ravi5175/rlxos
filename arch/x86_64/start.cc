#include "loader.h"

const char *EFI_MEMORY_TYPE_STRINGS[]
{
    "EfiReservedMemoryType",
    "EfiLoaderCode",
    "EfiLoaderData",
    "EfiBootServicesCode",
    "EfiBootServicesData",
    "EfiRuntimeServicesCode",
    "EfiRuntimeServicesData",
    "EfiConventionalMemory",
    "EfiUnusableMemory",
    "EfiACPIReclaimMemory",
    "EfiACPIMemoryNVS",
    "EfiMemoryMappedIO",
    "EfiMemoryMappedIOPortSpace",
    "EfiPalCode",
};

BootInfo::BootInfo(uint64_t address)
{
    size_t size = *(unsigned*) address;
    for( __tag__ = (multiboot_tag*) (address + 8);
         __tag__->type != MULTIBOOT_TAG_TYPE_END;
         __tag__ = (multiboot_tag*) ((multiboot_uint8_t*)__tag__
                    + ((__tag__->size + 7) & ~7)))
    {
        switch(__tag__->type) {
            case MULTIBOOT_TAG_TYPE_CMDLINE:
                this->cmdline = ((multiboot_tag_string*)__tag__)->string;
                break;
            
            case MULTIBOOT_TAG_TYPE_BOOT_LOADER_NAME:
                this->bootloader_id = ((multiboot_tag_string*)__tag__)->string;
                break;
            
            case MULTIBOOT_TAG_TYPE_BASIC_MEMINFO:
                this->memory_info = MemoryInfo(__tag__);
                break;
            
            case MULTIBOOT_TAG_TYPE_MMAP:
                this->memory_map_info = MemoryMapInfo(__tag__);
                break;
            
            case MULTIBOOT_TAG_TYPE_FRAMEBUFFER:
                this->framebuffer_info = FrameBufferInfo(__tag__);
                break;
        }
    }
}

extern "C" void* end_kernel;
extern "C" int main(BootInfo*);

extern "C" int
_start(uint64_t boot_info_addr, int magic_num)
{   
    process("initializing multiboot");
    print_info("MULTIBOOT ADDRESS = %x", boot_info_addr);
    print_info("MAGIC NUMBER = %x",magic_num);

    if (magic_num != MULTIBOOT2_BOOTLOADER_MAGIC)
    {
        PANIC("Invalid magic number = %x", (unsigned)magic_num);
    }

    if (boot_info_addr & 7) 
    {
        PANIC("unaligned mbi = %x", boot_info_addr);
    }

    BootInfo bootInfo(boot_info_addr);
    bootInfo.kernel_end_address = (uint64_t)&end_kernel;
    
    main(&bootInfo);

    for(;;);

    return 0;
}