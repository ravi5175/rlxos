#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include "vga.h"
#include "port.h"

static const size_t VGA_WIDTH = 80;
static const size_t VGA_HEIGHT = 25;

static size_t vga_row = 0;
static size_t vga_col = 0;
static uint8_t vga_color;

uint16_t* vga_buffer = (uint16_t*) 0xB8000;


void initialize_vga(void)
{
    vga_color = vga_entry_color(VGA_COLOR_WHITE, VGA_COLOR_BLACK);
    vga_clear();
}


void vga_setcolor(uint8_t color)
{
    vga_color = color;
}

void vga_putat(char c, uint8_t color, size_t x, size_t y)
{
    const uint16_t idx = y * VGA_WIDTH + x;
    vga_buffer[idx] = vga_entry(c, color);

    outb(0x3D4, 0x0F);
    outb(0x3D5, (uint8_t) ((idx + 1) & 0xFF));
	outb(0x3D4, 0x0E);
	outb(0x3D5, (uint8_t) (((idx + 1) >> 8) & 0xFF));
}

void vga_putchar(char c)
{
    if(c == '\n') {
        ++vga_row;
        vga_col = 0;
    } else if(c == '\t') {
        vga_col += 4;
    } else {
        vga_putat(c, vga_color, vga_col, vga_row);
        if (++vga_col == VGA_WIDTH) {
            vga_col = 0;
            if (++vga_row == VGA_HEIGHT) {
                vga_row = 0;
            }
        }
    }
}

void vga_write(const char* data, size_t size)
{
    for(size_t i = 0; i < size; i++)
        vga_putchar(data[i]);
}

void vga_print(const char* data)
{
    vga_write(data, strlen(data));
}

void vga_clear(void)
{
    for(size_t y = 0; y < VGA_HEIGHT; y++)
    {
        for(size_t x = 0; x < VGA_WIDTH; x++)
        {
            const size_t idx = y * VGA_WIDTH + x;
            vga_buffer[idx] = vga_entry(' ', vga_color);
        }
    }
}