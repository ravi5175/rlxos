#ifndef _ALLOC_H
#define _ALLOC_H

#include <stdint.h>
#include <stddef.h>

#define PAGE_SIZE 4096
#define PAGE_COUNT(address) address / PAGE_SIZE

class Allocator {
    public:
        virtual size_t allocate() = 0;
        virtual size_t delallocate(size_t) = 0;
};

class AreaFrameAllocator {
    private:
        uint64_t next_free_frame;
        
};

#endif