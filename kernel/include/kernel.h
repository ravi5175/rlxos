#ifndef _KERNEL_H
#define _KERNEL_H

#include <stdio.h>
#include <vga.h>
#include "multiboot.h"

#define fontcolor(color) vga_entry_color(color, VGA_COLOR_BLACK)

#define KENREL_VERSION "0.0.1"

#define PANIC(...)  printf("%iPANIC%i %s at %d\n", fontcolor(VGA_COLOR_RED), fontcolor(VGA_COLOR_WHITE), __FILE__, __LINE__);   \
                    printf(__VA_ARGS__); \
                    for(;;)

#define process(...) printf("\n%i: Process : %i", fontcolor(VGA_COLOR_LIGHT_CYAN), fontcolor(VGA_COLOR_WHITE)); \
                     printf(__VA_ARGS__); \
                     printf("...%i\n",fontcolor(VGA_COLOR_LIGHT_GREY))

#define info(...)    printf("\n%i: Info : %i", fontcolor(VGA_COLOR_LIGHT_BROWN), fontcolor(VGA_COLOR_WHITE)); \
                     printf(__VA_ARGS__); \
                     printf("...%i\n",fontcolor(VGA_COLOR_LIGHT_GREY))

#define print_info(...) printf("    %i", fontcolor(VGA_COLOR_DARK_GREY)); \
                          printf(__VA_ARGS__); \
                          printf("%i\n", fontcolor(VGA_COLOR_LIGHT_GREY));

#endif