#ifndef _MEM_H
#define _MEM_H

#include <stdint.h>
#include <stddef.h>
#include <multiboot.h>

namespace rlxos {
    class memory {
        private:
            size_t __start_loc__;
            uint64_t __memory_map_addr__;
            uint32_t __memory_map_length__;

            uint64_t __memory_location__;
            uint64_t __memory_size__;

        public:
            memory(uint64_t loc, uint64_t mmap_addr, uint32_t mmap_len)
                : __start_loc__(loc), __memory_map_addr__(mmap_addr),
                __memory_map_length__(mmap_len)
            {}

            void initialize();
    };
}

#endif