#include <loader.h>
#include <stdio.h>

extern "C"
int main(BootInfo* bootinfo) {

    bootinfo->memory_info.print();
    bootinfo->memory_map_info.print();
    bootinfo->framebuffer_info.print();
    
    return 0;
}