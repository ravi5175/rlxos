#ifndef _LIBC_STRING
#define _LIBC_STRING

#include <stddef.h>
#include <sys/cdefs.h>

#ifdef __cplusplus
extern "C" {
#endif

int memcmp(const void*, const void*, size_t);

void* memcpy(void* __restrict, const void* __restrict, size_t);

void* memmove(void*, const void*, size_t);

void* memset(void*, int, size_t);

size_t strlen(const char* str);

char* strtok(char* s, char d);

#ifdef __cplusplus
}
#endif

#endif